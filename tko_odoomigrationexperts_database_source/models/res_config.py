import random
import string

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class OdooMigrationExpertsConfig(models.TransientModel):
    _inherit = 'res.config.settings'

    migration_experts_token = fields.Char('Odoo Migration Experts Token')

    def generate_token(self):
        token = ''.join(random.choice(string.ascii_letters + string.digits) for i in range(256))
        if not self.env['ir.config_parameter'].sudo().get_param('migration_experts_token'):
            raise ValidationError('The token was deleted. '
                                  'You have to install Odoo Migrations Experts Source Database module again.')
        self.env['ir.config_parameter'].sudo().set_param('migration_experts_token', token)
        self.migration_experts_token = token

    @api.model
    def default_get(self, fields):
        res = super(OdooMigrationExpertsConfig, self).default_get(fields)
        res.update(migration_experts_token=self.env['ir.config_parameter'].sudo().get_param('migration_experts_token'))
        return res
