from odoo import models
from odoo.exceptions import AccessDenied


class ResUsers(models.Model):
    _inherit = 'res.users'

    def _check_credentials(self, password):
        try:
            token = self.env['ir.config_parameter'].sudo().get_param('migration_experts_token')
            if not token or len(token) < 256 or not password or password != token:
                raise AccessDenied()
        except AccessDenied:
            return super(ResUsers, self)._check_credentials(password)
