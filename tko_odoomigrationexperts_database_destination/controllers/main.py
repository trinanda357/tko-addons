from odoo import http
from odoo.http import request
from odoo.addons.tko_odoomigrationexperts_database_source.controllers.main import OdooMigrationExpertsSourceControllers


class OdooMigrationExpertsDestinationControllers(OdooMigrationExpertsSourceControllers):

    @http.route('/ome/execute', type='json', auth='public', csrf=False, website=True, sitemap=False)
    def run(self):
        data = []
        req = dict(request.jsonrequest)
        error = self._check_required_params(req, ['token', 'query'])
        if not error:
            query = req.get('query')
            try:
                request.env.cr.execute(query)
                data = request.env.cr.fetchall()
            except Exception as e:
                error = e
        return self._json_response(data=data, error=error)
