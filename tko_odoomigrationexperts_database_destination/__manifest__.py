# -*- coding: utf-8 -*-
{
    'name': 'Odoo Migration Experts Destination Database',
    'description': '''Migrate any Odoo version to any version from Odoo Migration Experts.
This module is to be installed in destination database. It implements read and write access for migration.''',
    'category': 'Technical',
    'version': '11.0.0.1',
    'sequence': 3,
    'author': 'Odoo Migration Experts',
    'license': 'OPL-1',
    'website': 'https://odoomigrationexperts.com',
    'support': 'info@odoomigrationexperts.com',
    'depends': [
        'tko_odoomigrationexperts_database_source',
    ],
    'data': [],
    'application': True,
    'installable': True,
    'auto_install': False,
}
