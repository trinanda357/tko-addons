from odoo import models, fields
from datetime import datetime

class ResPartner(models.Model):
    _inherit = 'res.partner'

    irs_year = fields.Char('IRS Year')

    def get_irs_amount(self):
        start_date = datetime.today().date().replace(day=1,month=1, year = int(self.irs_year))
        end_date = datetime.today().date().replace(day=31, month=12, year=int(self.irs_year))
        amount_paid = sum(self.env['account.invoice'].search([('state','=','paid'),('date_invoice','>=',start_date),('date_invoice','<=',end_date),('partner_id','=',self.id)]).mapped('amount_total'))
        return '%s €' %amount_paid
