# © 2019 TKOpen <https://tkopen.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'IRS Declararion',
    'summary': 'IRS Declararion Report',
    'description': '',
    'author': 'TKOpen',
    'category': 'Sales',
    'license': 'AGPL-3',
    'website': 'https://tkopen.com',
    'version': '12.0.1',
    'sequence': 1,
    'depends': [
    ],
    'data': [
        'report/irs_declaration_report_view.xml',
        'wizard/irs_declaration_view.xml',
    ],
    'external_dependencies': {
        'python': [],
    },
    'installable': True,
    'application': True,
    'auto_install': True,
}
