import math

from odoo import models, fields, api
from odoo.exceptions import ValidationError
import logging
_logger = logging.getLogger(__name__)


class AccountAnalyticAccount(models.Model):
    _inherit = 'account.analytic.account'

    state = fields.Selection([('d', 'Draft'), ('f', 'To Fix'), ('c', 'Confirmed')], default='d', track_visibility='onchange',
                             string='Stage')
    invoicing_day = fields.Integer('Invoicing Day', help='Used to compute Expected Invoice Qty at this date')
    invoice_ids = fields.Many2many('account.invoice', 'Invoices', compute='_get_invoices')

    def valid_recurring_contracts(self):
        """ Inherit this if you have different logic for valid recurring contracts """
        contracts = self.with_context(cron=True).search([
            ('recurring_invoices', '=', True)
        ])
        return contracts

    @api.model
    def cron_recurring_create_invoice(self):
        contracts = self.valid_recurring_contracts()
        for contract in contracts:
            try:
                contract.with_context(confirm_invoice=True).recurring_create_invoice()
                self._cr.commit()
            except Exception as e:
                _logger.error('A error encountered ...... : %s ' % e)
        return True

    @api.one
    def _get_invoices(self):
        self.invoice_ids = [(6, 0, self.recurring_invoice_line_ids.mapped('invoice_ids').ids)]

    @api.depends('recurring_next_date', 'date_end', 'recurring_invoices')
    def _compute_create_invoice_visibility(self):
        for contract in self:
            contract.create_invoice_visibility = contract.recurring_invoices

    ## SET correct quantity on invoice line in case we do not have enough quantity
    # left for last invoice happens with installments
    def _prepare_invoice_line(self, line, invoice_id):
        result = super(AccountAnalyticAccount, self)._prepare_invoice_line(line, invoice_id)
        remaining_invoice_qty = line.max_invoice_qty - line.total_invoiced_qty
        result.update({'quantity': min(remaining_invoice_qty, line.quantity)})
        return result

    @api.constrains('date_start', 'date_end')
    def validate_invoice_line(self):
        for line in self.recurring_invoice_line_ids:
            print("contract : %s : %s" % (self.date_start, self.date_end))
            print("line : %s : %s" % (line.date_start, line.date_end))
            if not line.display_type and line.date_start and line.date_end:
                if line.date_start < self.date_start or line.date_end > self.date_end:
                    raise ValidationError("%s must fall between %s - %s" % (
                        line.product_id.name, self.date_start, self.date_end))
        return True

    # Set line inactive
    @api.one
    def check_expiry_lines(self, line=False):
        for line in self.recurring_invoice_line_ids:
            line.set_line_status()
        return True

    # Set line inactive and validate invoice creation
    def verify_invoice_creation(self, line):
        line._get_invoicing_status()
        if line.total_invoiced_qty >= line.max_invoice_qty:
            return False
        self.check_expiry_lines(line)
        if line.date_end and line.recurring_next_date and line.date_end >= line.recurring_next_date:
            ## next recurring date must not be greater than line's expriy date if set
            return True
        ## next recurring date must not be greater than contract's expriy date if set
        elif line.analytic_account_id.date_end and line.recurring_next_date and line.analytic_account_id.date_end >= line.recurring_next_date:
            return True
        else:
            # line.set_inactive()
            return False

    @api.multi
    def confirm_recurring_invoices(self, invoices):
        for invoice in invoices:
            if invoice.company_id.confirm_recurring_invoice:
                invoice.action_invoice_open()
        return True

    @api.multi
    def recurring_create_invoice(self):
        """Create invoices from contracts

        :return: invoices created
        """
        self.check_expiry_lines()
        invoices = self.env['account.invoice']
        invoices_dict = {}
        for contract in self:
            for line in contract.recurring_invoice_line_ids.filtered(
                    lambda l: l.recurring_next_date <= fields.Date.today()):
                if line.invoicing_status in ['t','d'] and self.verify_invoice_creation(line):
                    if line.recurring_next_date not in invoices_dict.keys():
                        invoices_dict[line.recurring_next_date] = [line]
                    else:
                        invoices_dict[line.recurring_next_date].append(line)
            ## update next recurring date on lines
            for line in contract.recurring_invoice_line_ids.filtered(
                    lambda l: l.recurring_next_date <= fields.Date.today()):
                # compute from  today's date if not next date is not set
                old_date = fields.Date.from_string(line.recurring_next_date or fields.Date.today())
                new_date = old_date + contract.get_relative_delta(
                    line.recurring_rule_type, line.recurring_interval)

                if line.state == 'a':
                    if line.date_end and new_date <= line.date_end:
                        line.recurring_next_date = new_date
                    elif new_date <= line.analytic_account_id.date_end:
                        line.recurring_next_date = new_date
            for key, value in invoices_dict.items():
                # prepare invoice without invoice line and update date from last next recurring date
                invoice = self.env['account.invoice'].create(
                    contract._prepare_invoice())
                _logger.info("Invoice created .............%s"%invoice)
                invoice.date_invoice = False
                invoices |= invoice
                # set lines
                for line in value:
                    line.invoice_ids = [(4, invoice.id)]
                    invoice_line_vals = contract._prepare_invoice_line(line, invoice.id)
                    if invoice_line_vals:
                        self.env['account.invoice.line'].create(invoice_line_vals)
                invoices.compute_taxes()
            contract.recurring_invoice_line_ids._get_invoicing_status()
        #Confirm Invoice if configured to be Confirmed
        if self.env.context.get('confirm_invoice'):
            self.confirm_recurring_invoices(invoices)
        return invoices

    def set_draft(self):
        self.state = 'd'

    def set_confirm(self):
        self.recurring_invoices = True
        self.state = 'c'

    def set_fix(self):
        self.recurring_invoices = False
        self.state = 'f'

class AccountAnalyticAccountLine(models.Model):
    _inherit = 'account.analytic.invoice.line'

    display_type = fields.Selection([
        ('line_section', "Section"),
        ('line_note', "Note")], default=False, help="Technical field for UX purpose.")
    state = fields.Selection([('a', 'Active'), ('i', 'Inactive')], string=u'Subscription', default='a', copy=False,
                             required=True)
    invoicing_status = fields.Selection([('t', 'On Time'), ('d', 'Delayed'), ('f', 'Fully Invoiced'),('p','Fully Paid')], default='t',
                                        required=True, compute='_get_invoicing_status', store=False,
                                        string='Invoicing Status')
    max_invoice_qty = fields.Integer('To Created')
    total_invoiced_qty = fields.Float('Created', compute='_get_invoicing_status', store=True, )
    expected_invoice_qty = fields.Integer('Expected Invoices up to date', compute='get_expected_invoice_qty',
                                          help='Expected no of invoices to be genrated upto date')
    date_start = fields.Date('Date Start', default=fields.Date.context_today, copy=False, required=True)
    date_end = fields.Date('Date End', copy=False, required=True)
    recurring_next_date = fields.Date('Date of Next Invoice', default=fields.Date.context_today,
                                      copy=False, required=True)
    recurring_interval = fields.Integer(
        default=1,
        string='Repeat Every',
        help="Repeat every (Days/Week/Month/Year)",
        required=True,
    )
    recurring_rule_type = fields.Selection(
        [('daily', 'Day(s)'),
         ('weekly', 'Week(s)'),
         ('monthly', 'Month(s)'),
         ('monthlylastday', 'Month(s) last day'),
         ('yearly', 'Year(s)'),
         ],
        default='monthly',
        string='Recurrence',
        required=True,
        help="Specify Interval for automatic invoice generation.",
    )
    invoice_ids = fields.Many2many('account.invoice', 'account_analytic_invoice_line_rel', 'analytic_invoice_line_id',
                                   'invoice_id', string='Invoices')

    @api.one
    def set_line_status(self):
        if self.date_end < fields.Date.today():
            self.set_inactive()
        else:
            self.set_active()

    @api.constrains('date_start', 'date_end')
    def validate_invoice_line(self):
        self.set_line_status()
        contract_date_start = self.analytic_account_id.date_start
        contract_date_end = self.analytic_account_id.date_end
        if self.date_start and self.date_end and not self.display_type:
            if self.date_start < contract_date_start or self.date_end > contract_date_end:
                raise ValidationError("%s must fall between %s - %s" % (
                    self.product_id.name, contract_date_start, contract_date_end))
        return True

    @api.one
    @api.depends('invoice_ids', 'invoice_ids.state', 'invoice_ids.invoice_line_ids', 'date_start',
                 'date_end')  # ,'total_invoiced_qty')
    def _get_invoicing_status(self):
        invoices = self.invoice_ids.filtered(lambda l: l.state in ['draft', 'open', 'paid', 'in_payment'])
        invoiced_qty = 0
        for invoice in invoices:
            for line in invoice.invoice_line_ids:
                if line.product_id == self.product_id:
                    invoiced_qty += max(line.quantity, 1)
        self.total_invoiced_qty = invoiced_qty
        ## Fully Invoiced
        if self.total_invoiced_qty >= self.max_invoice_qty or int(self.discount) >= 100:
            self.invoicing_status = 'f'
        elif self.total_invoiced_qty < self.expected_invoice_qty:
            self.invoicing_status = 'd'
        else:
            self.invoicing_status = 't'

        ### Fully Paid
        if self.discount == 100 or len(invoices) and invoices.filtered(lambda l: l.state in ['paid']):
            paid_invoices = invoices.filtered(lambda l: l.state in ['paid'])
            invoice_lines = self.env['account.invoice.line'].search([('invoice_id','in',paid_invoices.ids),('product_id','=', self.product_id.id)])
            if self.product_id in invoice_lines.mapped('product_id'):
                ## If all quantities have been paid
                if sum(invoice_lines.mapped('quantity')) >= self.max_invoice_qty:
                    self.invoicing_status = 'p'



    @api.one
    def get_expected_invoice_qty(self):
        """
        This will compute expected invoices to be generated upto a given date invoicing_day (default =1)
        :return:
        """
        periods = {'daily': 1, 'weekly': 7, 'monthly': 30, 'monthlylastday': 90, 'yearly': 365}
        invoicing_day = self.analytic_account_id.invoicing_day or 1
        today = fields.Date().today()
        validation_date = fields.Date().today()
        if today.day < invoicing_day:
            ## compute for previous interval
            validation_date = today + self.analytic_account_id.get_relative_delta(
                self.recurring_rule_type, -1 * self.recurring_interval)
            validation_date = validation_date.replace(day=invoicing_day)
        total_days = (validation_date - (
                self.date_start + self.analytic_account_id.get_relative_delta(self.recurring_rule_type,
                                                                              -1 * self.recurring_interval))).days
        interval = int((self.date_end - self.date_start).days / periods[self.recurring_rule_type])  ## 10, 12
        frequency = interval / (self.max_invoice_qty or 1.0)
        expected_invoice_qty = math.ceil(
            (today - self.date_start).days / periods[self.recurring_rule_type] / (frequency or 1))
        if expected_invoice_qty > self.max_invoice_qty:
            expected_invoice_qty = self.max_invoice_qty
        self.expected_invoice_qty = max(expected_invoice_qty, 0)

    def set_inactive(self):
        self.state = 'i'

    def set_active(self):
        self.state = 'a'
